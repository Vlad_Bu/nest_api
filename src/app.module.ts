import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TasksModule } from './tasks/tasks.module';
import * as process from "process";

const username = process.env.MYSQL_USER || "root";
const password = process.env.MYSQL_PASSWORD || "root";


@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: 'localhost',
      port: 6603,
      username,
      password,
      database: 'course_mysql',
      entities: [__dirname + '/**/*.entity{.ts,.js}'],
      synchronize: true,
    }),
    TasksModule,
 ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
