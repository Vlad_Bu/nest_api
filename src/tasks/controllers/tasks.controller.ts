import { Controller, Post, Get, Body, Param, Put, Delete } from '@nestjs/common';
import { Tasks } from '../entities/tasks.entity';
import { TasksService } from '../services/tasks.service';

@Controller('tasks')
export class TasksController {
  constructor(private tasksService : TasksService){}

  @Get()
  index(): Promise<Tasks[]> {
    return this.tasksService.findAll();
  }
  
  @Put(':id/update')
  update(@Param() id, @Body() body: Tasks) {
    return this.tasksService.update(id,body);
  }

  @Post('/create')
    async create(@Body() tasksData: Tasks): Promise<any> {
      return this.tasksService.create(tasksData);
    }  

  @Delete(':id/delete')
    async delete(@Param('id') id): Promise<any> {
      return this.tasksService.delete(id);
  }    
}