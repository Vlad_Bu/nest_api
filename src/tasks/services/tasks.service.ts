import { Injectable } from '@nestjs/common';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Tasks } from '../entities/tasks.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { UpdateResult, DeleteResult } from  'typeorm';

@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(Tasks)
        private tasksRepository: Repository<Tasks>,
    ) { }

  async  findAll(): Promise<Tasks[]> {
    return await this.tasksRepository.find();
  }

  async update(id, task: Tasks): Promise<UpdateResult> {      
    return await this.tasksRepository.update(id, task);
  }

  async  create(task: Tasks): Promise<Tasks> {
    return await this.tasksRepository.save(task);
  }

  async  delete(id): Promise<DeleteResult> {
    return await this.tasksRepository.delete(id);
  }


}