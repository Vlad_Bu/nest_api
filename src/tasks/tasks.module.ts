import { Module } from '@nestjs/common';
import { TasksService } from './services/tasks.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tasks } from './entities/tasks.entity';
import { TasksController } from './controllers/tasks.controller';


@Module({
  controllers: [TasksController],
  providers: [TasksService],
  imports: [
    TypeOrmModule.forFeature([Tasks])
  ]
})
export class TasksModule {}