import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Tasks {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({unique: false})
  title: string;

  @Column({unique: false, default: false})
  isCompleted: boolean;
}