import {MigrationInterface, QueryRunner} from "typeorm";

export class CreateTasksTable1599723599074 implements MigrationInterface {
    name = 'CreateTasksTable1599723599074'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("CREATE TABLE `tasks` (`id` int NOT NULL AUTO_INCREMENT, `title` varchar(255) NOT NULL, `isCompleted` tinyint NOT NULL, `isDeleted` tinyint NULL, PRIMARY KEY (`id`)) ENGINE=InnoDB");
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query("DROP TABLE `tasks`");
    }

}
