
const process = require('process');

const username = process.env.MYSQL_USER || "root";
const password = process.env.MYSQL_PASSWORD || "root";

module.exports = {
  "type": "mysql",
  "host": "localhost",
  "port": 6603,
  username,
  password,
  "database": "course_mysql",
  "synchronize": true,
  "dropSchema": false,
  "logging": true,
  "entities": [__dirname + "/src/**/*.entity.ts", __dirname + "/dist/**/*.entity.js"],
  "migrations": ["migrations/**/*.ts"],
  "subscribers": ["subscriber/**/*.ts", "dist/subscriber/**/.js"],
  "cli": {
    "entitiesDir": "src",
    "migrationsDir": "migrations",
    "subscribersDir": "subscriber"
  }
}